import UIKit


//Создать класс родитель и 2 класса наследника
class Animal{
    func speak() -> String{
//        print("Some Noise", terminator: " ")
        return "Some Noise"
    }
//    var paws : Int = 0
}

class Worm : Animal{
    
}

class Elephant : Animal{
    override func speak() -> String{
//        print(super.speak() + " of big paws")
        return super.speak() + " of big paws"
    }
}

class Dog : Animal{
    override func speak() -> String{
//        print("gav-gav")
        return "gav-gav"
    }
}
var someElephant = Elephant()
print(someElephant.speak())
var someDog = Dog()
print(someDog.speak())





//Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House{
    var name : String
    var width : Int
    var height : Int
    
    func create() -> Int {
        return width * height
    }
    
    func destroy() -> Void {
        print("House \(name) was destroyed")
    }
    
    init(name: String, width : Int, height : Int){
        self.name = name
        self.width = width
        self.height = height
    }
    deinit {
        destroy()
    }
}

var house1 = House(name: "My house", width: 15, height: 20)
house1.destroy()



//Создайте класс с методами, которые сортируют массив учеников по разным параметрам
class Student{
    
    var students : [String] = []

    func sortByLen(){
        students = students.sorted(by: {$0.count > $1.count})
    }
    func sortByLAlphabet(){
        students = students.sorted(by: <)
    }
    init(str : String){
        self.students = str.capitalized.components(separatedBy: " ")
    }
}

var group = Student(str: "CHvyrov PosveSH Derend Verem Tatarchuk<3 smirnov")

print(group.students)

group.sortByLen()

print(group.students)

group.sortByLAlphabet()

print(group.students)


//Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
class StudentClass{
    var name : String
    var age : Int
    init(){
        name = "No name"
        age = 20
    }
    init(name: String, age : Int){
        self.name = name
        self.age = age
    }
}

struct StudentStruct{
    var name : String
    var age : Int
    
}

let stStudent1 = StudentClass(name: "Paul", age: 23)

var studentSt = StudentStruct(name: "Sam", age: 24)
//Создайте класс с методами, которые сортируют массив учеников по разным параметрам

// У структур не нужно делать конструкторы т.к. они автоматические
// У структур нет наследования, в некоторых языках есть
// Структуры работают через копирование т.е. valueType,
//а классы через ссылки т.е. referenсeType


//Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
//Сохраняйте комбинации в массив
//Если выпала определённая комбинация - выводим соответствующую запись в консоль
//Результат в консоли примерно такой: 'У вас бубновый стрит флеш'.
enum Suit{
    case Diamonds
    case Clubs
    case Hearts
    case Spades
}
enum Value : Int{
    case Two = 2
    case Three = 3
    case Four = 4
    case Five = 5
    case Six = 6
    case Seven = 7
    case Eight = 8
    case Nine = 9
    case Ten = 10
    case Jack = 11
    case Queen = 12
    case King = 13
    case Ace = 1
}


class Combination{
    
    var dictValue : [Value : Int] = [:]
    var dictSuit : [Suit : Int] = [:]
    
    
    
    init(table : [Card]) {
        for i in table{
            if dictValue[i.value] == nil{
                dictValue[i.value] = 1
            }
            else{
                dictValue[i.value]! += 1
            }
            if dictSuit[i.suit] == nil{
                dictSuit[i.suit] = 1
            }
            else{
                dictSuit[i.suit]! += 1
            }
        }
    }
    //то, что идет дальше
    //выглядит как нечитаемое гавно
    //думаю что можно было просто сделать через closure хз)
    func result(){
        
        if(checkFour()){
            return
        }
        if(checkSetAndHouse()){
            return
        }
        if(checkPair()){
            return
        }
        var isFlush = checkFlush()
        
        var isStraight = checkStraight()
        
        var isStraightAce = checkStraightAce()
        
        if isFlush.0 && isStraightAce{
            print("You have flush royal")
            return
        }
        
        if isFlush.0 && isStraight.0{
            print("You have straight flush on: \(isFlush.1!) from: \(isStraight.1!) to: \(isStraight.2!)")
            return
        }
        if isFlush.0{
            print("You have flush on: \(isFlush.1!)")
            return
        }
        if isStraight.0{
            print("You have straight from: \(isStraight.1!) to: \(isStraight.2!)")
            return
        }
        
    }
    
    func checkFour() -> Bool{
        if dictValue.contains(where: {$0.value == 4}){
            for i in dictValue{
                if i.value == 4{
                    print("You have 4: \(i.key) of a kind")
                    return true
                }
            }
        }
        return false
    }
    
    func checkSetAndHouse() -> Bool{
        if dictValue.contains(where: {$0.value == 3}){
            var set : Value = .Two //хз почему просит инициализировать
            
            if dictValue.contains(where: {$0.value == 2}){
                var house2 : Value = .Three //хз почему просит инициализировать
                for i in dictValue{
                    if i.value == 3{
                        set = i.key
                        continue
                    }
                    if i.value == 2{
                        house2 = i.key
                        continue
                    }
                }
                print("You have full house on 3: \(set) and 2: \(house2)")
                return true
            }
            else{
                for i in dictValue{
                    if i.value == 3{
                        set = i.key
                        break
                    }
                }
                print("You have set on 3: \(set)")
                return true
            }
        }
        return false
    }
    
    func checkPair () -> Bool{
        if dictValue.contains(where: {$0.value == 2}){
            var pair : Value = .Two
            
            if dictValue.count == 3{
                for i in dictValue{
                    if i.value == 2{
                        pair = i.key
                        break
                    }
                }
                for i in dictValue{
                    if i.value == 2 && i.key != pair{
                        print("You have two pairs on 2: \(pair), and 2: \(i.key)")
                        return true
                    }
                }
            }
            else{
                for i in dictValue{
                    if i.value == 2{
                        print("You have pair on 2: \(i.key)")
                        return true
                    }
                }
            }
            
        }
        return false
    }
    
    func checkFlush() -> (Bool, Suit?){
        if dictSuit.contains(where: {$0.value == 5}){
            for i in dictSuit{
                if i.value == 5{
                    return (true, i.key)
                }
            }
        }
        return (false,nil)
    }

    func getValue() -> [Int]{
        var straight : [Int] = []
        for i in dictValue{
            straight.append(i.key.rawValue)
        }
        straight.sort()
        return straight
    }

    func checkStraight() -> (Bool, Value?, Value?){
        
        let straight = getValue()
        for i in 0..<4{
            if straight[i] + 1 != straight[i+1]{
                return (false, nil, nil)
            }
        }
        var min : Value = .Two
        var max : Value = .Two
        for i in dictValue{
            if i.key.rawValue == straight[0]{
                min = i.key
                break
            }
        }
        for i in dictValue{
            if i.key.rawValue == straight[4]{
                max = i.key
                break
            }
    
        }
        return (true, min, max)
    }
    
    func checkStraightAce() -> Bool{
        let straight = getValue()
        if straight[0] == 1 &&
            straight[1] == 10 &&
            straight[2] == 11 &&
            straight[3] == 12 &&
            straight[4] == 13 {
            return true
        }
        else{
            return false
        }
    }
}

class Card{
    var suit : Suit
    var value : Value
    
    
    
    init (){
        self.suit = .Diamonds
        self.value = .Two
    }
    init(suit : Suit, value : Value){
        self.suit = suit
        self.value = value
    }
}

var firstCard = Card(suit: .Diamonds , value: .Nine)
var secondCard = Card(suit: .Diamonds, value: .Jack)
var thirdCard = Card(suit: .Diamonds, value: .Queen)
var fourthCard = Card(suit: .Diamonds, value: .Ten)
var fifthCard = Card(suit: .Diamonds, value: .King)
var table = [firstCard, secondCard, thirdCard, fourthCard, fifthCard]

var combination = Combination(table: table)
//combination.sameValue()



combination.result()
